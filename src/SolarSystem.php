<?php

namespace Gnemes\MercadoLibre\SolarSystem;

use \Gnemes\MercadoLibre\SolarSystem\PlanetFactory;
use \Gnemes\MercadoLibre\SolarSystem\Helper\Geometry;
use \Gnemes\MercadoLibre\SolarSystem\Helper\Config;
use \Doctrine\DBAL\DriverManager;

class SolarSystem
{
    /**
     * @var
     */
    private $currentWeatherCondition;

    /**
     * Planet
     * @var \Gnemes\MercadoLibre\SolarSystem\Planet[]
     */
    private $planets;

    /**
     * Planet factory
     * @var \Gnemes\MercadoLibre\SolarSystem\PlanetFactory
     */
    private $planetFactory;

    /**
     * @var Geometry
     */
    private $geometryHelper;

    /**
     * @var Config
     */
    private $configHelper;

    /**
     * Current dat
     * @var int
     */
    private $currentDay = 0;

    /**
     * Triangle perimeter
     * @var float
     */
    private $trianglePerimeter = 0;

    /**
     * SolarSystem constructor.
     */
    public function __construct()
    {
        // Geometry Helper instance
        $this->geometryHelper = new Geometry();

        // Config Helper instance
        $this->configHelper = new Config();

        // Instance Planet Factory
        $this->planetFactory = new PlanetFactory($this->geometryHelper, $this->configHelper);

        // Create planets
        $this->createPlanets();
    }

    /**
     * Load solar system planets
     * @return $this
     */
    private function createPlanets()
    {
        // Create planets
        $this->planets = $this->planetFactory->createSolarSystemPlanets();
        return $this;
    }

    /**
     * Get planets rectangular coordinates
     */
    private function getPlanetsRectangularCoordinates()
    {
        // Init vars
        $coordinates = [];

        if (count($this->planets) > 0) {
            foreach ($this->planets as $planet) {
                // Get each planet rectangular coordinates
                $coordinates[] = $planet->getRectangularCoordinates();
            }
        }

        return $coordinates;
    }

    /**
     * Move solar system to day
     * @param $day
     *
     */
    public function moveToDay($day)
    {
        if ( (count($this->planets) > 0) && (is_int($day)) && ($day > 0) ) {
            foreach ($this->planets as $planet) {
                $planet->recalculatePositionByDay($day);
            }
        }
    }

    /**
     * Return planets ordered by sun distance
     * @return array
     */
    public function getPlanets()
    {
        // Init vars
        $planets = [];

        if (count($this->planets) > 0) {
            foreach ($this->planets as $planet) {
                $p = [];
                $coordinates = $planet->getRectangularCoordinates();

                $p['name'] = $planet->getName();
                $p['posX'] = $coordinates[0];
                $p['posY'] = $coordinates[1];
                $p['sunDistance'] = $planet->getSunDistance();
                $planets[] = $p;
            }
        }

        return $planets;
    }

    public function getPrecipitationMaxDays()
    {
        $dbInfo = $this->configHelper->get("database");
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = array(
            'dbname' => $dbInfo['database'],
            'user' => $dbInfo['username'],
            'password' => $dbInfo['password'],
            'host' => $dbInfo['hostname'],
            'port' => 3306,
            'charset' => 'utf8',
            'driver' => 'pdo_mysql',
        );
        $conn = DriverManager::getConnection($connectionParams, $config);
        $query = <<<EOQ
SELECT *
  FROM precipitaciones p1
 WHERE p1.perimetro = (SELECT MAX(p2.perimetro) FROM precipitaciones p2)
 ORDER BY p1.dia ASC
EOQ;

        $sth = $conn->query($query);
        $precipitationsMax = $sth->fetchAll();

        return $precipitationsMax;
    }

    public function getWeatherConditionQtyByWeather()
    {
        $dbInfo = $this->configHelper->get("database");
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = array(
            'dbname' => $dbInfo['database'],
            'user' => $dbInfo['username'],
            'password' => $dbInfo['password'],
            'host' => $dbInfo['hostname'],
            'port' => 3306,
            'charset' => 'utf8',
            'driver' => 'pdo_mysql',
        );
        $conn = DriverManager::getConnection($connectionParams, $config);

        $wheather = [
            'Lluvia',
            'Sequia',
            'Condiciones optimas de presion y temperatura'
        ];

        $sum = 0;
        $quantities = [];
        foreach ($wheather as $w)
        {
            $query = <<<EOQ
SELECT count(1) as days
  FROM clima
 WHERE clima = ?
EOQ;

            $sth = $conn->executeQuery($query, array($w));
            $res = $sth->fetch();

            // Days quantity sum
            $sum += (int)$res['days'];

            // Days per weather condition
            $quantities[$w] = (int)$res['days'];
        }

        // Get Normal days quantity
        $quantities['Normal'] = $this->getDaysQuantity() - $sum;

        return $quantities;
    }

    /**
     * Get total amount of days of the simulation
     * @return int
     */
    private function getDaysQuantity()
    {
        /**
         * Hardcoded for ten years
         */
        $years = 10;
        $daysPerYear = 360;
        return ($years * $daysPerYear);
    }

    /*********************************/
    /** Weather condition functions **/
    /*********************************/

    /**
     * Set current weather condition
     */
    public function getCurrentWeatherCondition()
    {
        $this->currentWeatherCondition = 'Normal';
        $this->trianglePerimeter = 0;

        if ($this->planetsAligned()) {
            $this->currentWeatherCondition = 'Condiciones optimas de presion y temperatura';

            if ($this->planetsAlignedWithTheSun()) {
                $this->currentWeatherCondition = 'Sequia';
            }
        } else if ($this->sunInsidePlanetsTriangle()) {
            $this->currentWeatherCondition = 'Lluvia';
            $this->trianglePerimeter = $this->calculatePlanetsTrianglePerimeter();
        }

        return $this->currentWeatherCondition;
    }

    public function getPlanetsTrianglePerimeter()
    {
        return $this->trianglePerimeter;
    }

    private function calculatePlanetsTrianglePerimeter()
    {
        $perimeter = 0;

        // Get planets coordinates
        $planetsCoordinates = $this->getPlanetsRectangularCoordinates();

        // Check if a triangle (three dots). If it is not, return false.
        if (count($planetsCoordinates) == 3) {
            list($x1, $y1) = array_pop($planetsCoordinates);
            list($x2, $y2) = array_pop($planetsCoordinates);
            list($x3, $y3) = array_pop($planetsCoordinates);

            // Calculate triangle perimeter
            $perimeter = $this->geometryHelper->calculateTrianglePerimeter(
                $x1, $y1, $x2, $y2, $x3, $y3
            );
        }

        return $perimeter;
    }

    /**
     * Check if sun is inside planets triangle
     * @return bool
     */
    private function sunInsidePlanetsTriangle()
    {
        // Init vars
        $precipitations = false;

        // Get planets coordinates
        $planetsCoordinates = $this->getPlanetsRectangularCoordinates();

        // Check if a triangle (three dots). If it is not, return false.
        if (count($planetsCoordinates) == 3) {
            list($x1, $y1) = array_pop($planetsCoordinates);
            list($x2, $y2) = array_pop($planetsCoordinates);
            list($x3, $y3) = array_pop($planetsCoordinates);

            // Sun coordinates
            $x = 0;
            $y = 0;

            // Check if dot is inside planets triangle
            $precipitations = $this->geometryHelper->dotInsideTriangle(
                $x1, $y1, $x2, $y2, $x3, $y3, $x, $y
            );
        }

        return $precipitations;
    }

    /**
     * Validate if planets are aligned
     *
     * @return bool
     */
    private function planetsAligned()
    {
        // Get planets coordinates
        $planetsCoordinates = $this->getPlanetsRectangularCoordinates();

        return $this->geometryHelper->areDotsAligned($planetsCoordinates);
    }

    /**
     * Validate if planets are aligned with the sun too
     *
     * @return bool
     */
    private function planetsAlignedWithTheSun()
    {
        // Get planets coordinates
        $planetsCoordinates = $this->getPlanetsRectangularCoordinates();

        // Sun coordinates
        $sunCoordinates = [0,0];

        $planetsCoordinates[] = $sunCoordinates;

        return $this->geometryHelper->areDotsAligned($planetsCoordinates);
    }

    /*********************************/


    public function currentState($web = false)
    {
        $eol = $web ? '<br/>' : "\n";
        echo "-----------------------".$eol;
        echo "Current day in the solar system: ".$this->currentDay.$eol;
        echo "-----------------------".$eol;

        if (count($this->planets) > 0) {
            foreach ($this->planets as $planet) {
                $planet->getPlanetInfo($web);
            }
        }
    }
}
