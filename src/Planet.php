<?php


namespace Gnemes\MercadoLibre\SolarSystem;


class Planet
{
    /**
     * Planet name
     * @var int
     */
    private $name;

    /**
     * Angular Speed
     * @var int
     */
    private $angularSpeed;

    /**
     * Distance to the sun
     * @var int
     */
    private $sunDistance;

    /**
     * Initial angle
     * @var int
     */
    private $initialAngle;

    /**
     * Current angle for current day
     * @var int
     */
    private $currenteAngle;

    /**
     * Current day number
     * @var int
     */
    private $currentDay;

    /**
     * Polar coordinates
     * @var []
     */
    private $polarCoordinates = [];

    /**
     * Rectangular coordinates
     * @var []
     */
    private $rectangularCoordinates = [];

    /**
     * Geometry helper
     * @var \Gnemes\MercadoLibre\SolarSystem\Helper\Geometry
     */
    private $geometryHelper;

    public function __construct(
        $angularSpeed,
        $initialAngle,
        $name,
        $sunDistance,
        $geometryHelper
    ) {
        // Geometry helper
        $this->geometryHelper = $geometryHelper;

        $this->setAngularSpeed($angularSpeed)
            ->setInitialAngle($initialAngle)
            ->setName($name)
            ->setSunDistance($sunDistance)
            ->setCurrentDay(0);

        // Set current angle
        $this->setCurrentAngle($this->getInitialAngle());

        // Recalculate position
        $this->recalculatePosition();
    }

    /**
     * Recalculate position based in current angle
     */
    private function recalculatePosition()
    {
        // Set polar coordinates
        $this->setPolarCoordinates(
            $this->getSunDistance(),
            $this->getCurrentAngle()
        );

        // Get rectangular coordinates
        $this->polar2rectangularCoordinates(
            $this->getPolarCoordinates()
        );
    }

    /**
     * Getter Angular Speed
     *
     * @return int
     */
    public function getAngularSpeed()
    {
        return $this->angularSpeed;
    }

    /**
     * Setter Angular Speed
     *
     * @param int $angularSpeed
     * @return $this
     */
    public function setAngularSpeed($angularSpeed)
    {
        $this->angularSpeed = $angularSpeed;
        return $this;
    }

    /**
     * Getter Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter Name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Getter SunDistance
     *
     * @return int
     */
    public function getSunDistance()
    {
        return $this->sunDistance;
    }

    /**
     * Setter SunDistance
     *
     * @param int $sunDistance
     * @return $this
     */
    public function setSunDistance($sunDistance)
    {
        $this->sunDistance = $sunDistance;
        return $this;
    }

    /**
     * Getter InitialAngle
     *
     * @return int
     */
    public function getInitialAngle()
    {
        return $this->initialAngle;
    }

    /**
     * Setter InitialAngle
     *
     * @param int $initialAngle
     * @return $this
     */
    public function setInitialAngle($initialAngle)
    {
        $this->initialAngle = $initialAngle;
        return $this;
    }

    /**
     * Getter CurrentDay
     *
     * @return int
     */
    public function getCurrentDay()
    {
        return $this->currentDay;
    }

    /**
     * Setter Current Day
     *
     * @param int $dayNumber
     * @return $this
     */
    public function setCurrentDay($dayNumber)
    {
        $this->currentDay = $dayNumber;
        return $this;
    }

    /**
     * Getter current angle
     *
     * @return int
     */
    public function getCurrentAngle()
    {
        return $this->currenteAngle;
    }

    /**
     * Setter current angle
     * @param $currentAngle
     * @return $this
     */
    public function setCurrentAngle($currentAngle)
    {
        $this->currenteAngle = $currentAngle;
        return $this;
    }

    /**
     * Get polar coordinates
     * @return array
     */
    public function getPolarCoordinates()
    {
        return $this->polarCoordinates;
    }

    /**
     * Set polar coordinates
     * @param $radius
     * @param $angle
     * @return $this
     */
    public function setPolarCoordinates($radius, $angle)
    {
        $this->polarCoordinates = $this->geometryHelper->createPolarCoordinates($radius, $angle);
        return $this;
    }

    /**
     * Set rectangular coordinates using planets polar coordinates
     * @return $this
     */
    public function polar2rectangularCoordinates($polarCoordinates)
    {
        if (count($polarCoordinates) > 0) {
            $this->rectangularCoordinates = $this->geometryHelper->polar2rectangularCoordinates(
                $polarCoordinates
            );
        }
        return $this;
    }

    /**
     * Getter rectangular coordinates
     * @return array
     */
    public function getRectangularCoordinates()
    {
        return $this->rectangularCoordinates;
    }

    /**
     * Recalculate position by day
     * @param int $day
     */
    public function recalculatePositionByDay($day = 0)
    {
        // Set current day
        $this->setCurrentDay($day);

        // Set current angle
        $this->setCurrentAngle($this->calculateCurrentAngleByCurrentDay());

        // Recalculate position
        $this->recalculatePosition();
    }

    /**
     * Recalculate angle by current day
     * @return int
     */
    private function calculateCurrentAngleByCurrentDay()
    {
        $currentAngle = $this->getInitialAngle() + $this->getAngularSpeed() * $this->getCurrentDay();

        while ($currentAngle > 360) {
            $currentAngle = $currentAngle - 360;
        }

        while ($currentAngle < 0) {
            $currentAngle = $currentAngle + 360;
        }
/*
        if ($currentAngle > 360) {
            $turns = floor($currentAngle / 360);
            $currentAngle = $currentAngle - $turns * 360;
        } else if ($currentAngle < 0) {
            $turns = floor($currentAngle / -360);
            $currentAngle = $currentAngle + $turns * 360;
        }
*/
        return $currentAngle;
    }

    /** Debug **/

    public function getPlanetInfo($web = false)
    {
        $eol = $web ? '<br/>' : "\n";
        echo "Name: ".$this->getName().$eol;
        echo "Angular Speed: ".$this->getAngularSpeed().$eol;
        echo "Sun Distance: ".$this->getSunDistance().$eol;
        echo "Initial angle: ".$this->getInitialAngle()." degrees".$eol;
        echo "Current angle: ".$this->getCurrentAngle()." degrees".$eol;
        $polarCoordinates = $this->getPolarCoordinates();
        echo "Polar coordinates: ".$polarCoordinates[0]." radius - ".$polarCoordinates[1]." radians angle.".$eol;
        $renctangularCoordinates = $this->getRectangularCoordinates();
        echo "Rectangular coordinates: ".$renctangularCoordinates[0]." X - ".$renctangularCoordinates[1]." Y.".$eol;
        echo "-----------------------".$eol;
    }

}