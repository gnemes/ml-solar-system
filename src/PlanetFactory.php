<?php

namespace Gnemes\MercadoLibre\SolarSystem;

use Gnemes\MercadoLibre\SolarSystem\Planet;
use Doctrine\DBAL\DriverManager;

class PlanetFactory
{
    /**
     * @var \Gnemes\MercadoLibre\SolarSystem\Helper\Geometry
     */
    private $geometryHelper;

    /**
     * @var \Gnemes\MercadoLibre\SolarSystem\Helper\Config
     */
    private $configHelper;

    /**
     * PlanetFactory constructor.
     * @param $geometryHelper
     */
    public function __construct($geometryHelper, $configHelper)
    {
        $this->geometryHelper = $geometryHelper;
        $this->configHelper = $configHelper;
    }

    public function getPlanetsInformation()
    {
        $dbInfo = $this->configHelper->get("database");
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = array(
            'dbname' => $dbInfo['database'],
            'user' => $dbInfo['username'],
            'password' => $dbInfo['password'],
            'host' => $dbInfo['hostname'],
            'port' => 3306,
            'charset' => 'utf8',
            'driver' => 'pdo_mysql',
        );
        $conn = DriverManager::getConnection($connectionParams, $config);

        $query = <<<EOQ
SELECT name, initial_angle AS initialAngle, angular_speed AS angularSpeed, sun_distance AS sunDistance 
  FROM planeta
EOQ;
        $sth = $conn->query($query);
        $planets = $sth->fetchAll();

        // This information could be retrieved from database
        /*
        $planets = [
            0 => [
                "name" => "Vulcano",
                "initialAngle" => 90,
                "angularSpeed" => 5,
                "sunDistance" => 1000
            ],
            1 => [
                "name" => "Ferengi",
                "initialAngle" => 90,
                "angularSpeed" => -1,
                "sunDistance" => 500
            ],
            2 => [
                "name" => "Betasoide",
                "initialAngle" => 90,
                "angularSpeed" => -3,
                "sunDistance" => 2000
            ]
        ];
        */

        return $planets;
    }

    /**
     * Create solar system planets
     *
     * @return \Gnemes\MercadoLibre\SolarSystem\Planet[]
     */
    public function createSolarSystemPlanets()
    {
        // Init vars
        $planets = [];

        // Get planets information
        $planetsInfo = $this->getPlanetsInformation();

        // Create planets
        foreach ($planetsInfo as $planetInfo) {
            $planet = new Planet(
                $planetInfo['angularSpeed'],
                $planetInfo['initialAngle'],
                $planetInfo['name'],
                $planetInfo['sunDistance'],
                $this->geometryHelper
            );

            $planets[] = $planet;
        }

        return $planets;
    }
}