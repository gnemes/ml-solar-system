<?php

namespace Gnemes\MercadoLibre\SolarSystem\Api;

use \Gnemes\MercadoLibre\SolarSystem\SolarSystem;

class SolarSystemState
    extends \Gnemes\MercadoLibre\SolarSystem\Api\Api
{
    protected $User;

    public function __construct($request, $origin) {
        parent::__construct($request);
/*
        // Abstracted out for example
        $APIKey = new Models\APIKey();
        $User = new Models\User();

        if (!array_key_exists('apiKey', $this->request)) {
            throw new Exception('No API Key provided');
        } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
            throw new Exception('Invalid API Key');
        } else if (array_key_exists('token', $this->request) &&
            !$User->get('token', $this->request['token'])) {

            throw new Exception('Invalid User Token');
        }

        $this->User = $User;
*/
    }

    protected function state($args)
    {
        if ($this->method == 'GET') {
            if (count($args) > 1) {
                return "Invalid arguments number.";
            }

            // Init vars
            $solarSystemState = [];

            // Solar system instance
            $solarSystem = new SolarSystem();

            // Get day from parameters
            $day = (int) $args[0];

            // Move solar system to given day
            $solarSystem->moveToDay($day);

            // Get planets
            $solarSystemState['planets'] = $solarSystem->getPlanets();

            // Weather
            $solarSystemState['weather'] = $solarSystem->getCurrentWeatherCondition();

            // Get day
            $solarSystemState['day'] = $day;

            return $solarSystemState;
        } else {
            return "Only accepts GET requests";
        }
    }

    protected function clima($args)
    {
        if ($this->method == 'GET') {
            if (count($args) > 1) {
                return "Invalid arguments number.";
            }

            // Init vars
            $solarSystemState = [];

            // Solar system instance
            $solarSystem = new SolarSystem();

            // Get day from parameters
            $day = (int) $args[0];

            // Move solar system to given day
            $solarSystem->moveToDay($day);

            // Get day
            $solarSystemState['dia'] = $day;

            // Weather
            $solarSystemState['clima'] = $solarSystem->getCurrentWeatherCondition();

            return $solarSystemState;
        } else {
            return "Only accepts GET requests";
        }
    }

    /**
     * Get the weather for all days
     */
    protected function full() {
        if ($this->method == 'GET') {
            $configHelper = new \Gnemes\MercadoLibre\SolarSystem\Helper\Config();

            $dbInfo = $configHelper->get("database");
            $config = new \Doctrine\DBAL\Configuration();

            $connectionParams = array(
                'dbname' => $dbInfo['database'],
                'user' => $dbInfo['username'],
                'password' => $dbInfo['password'],
                'host' => $dbInfo['hostname'],
                'port' => 3306,
                'charset' => 'utf8',
                'driver' => 'pdo_mysql',
            );
            $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

            $query = <<<EOQ
SELECT dia, clima
  FROM clima
EOQ;
            $sth = $conn->query($query);
            $weather = $sth->fetchAll();

            if (count($weather) > 0) {
                return $weather;
            } else {
                return "Clima table is empty. Please, re-run the job to fill the table.";
            }
        } else {
            return "Only accepts GET requests";
        }
    }

    protected function orbit()
    {
        if ($this->method == 'POST') {

            // Get args from request
            $args = $this->request;
            if (count($args) != 2) {
                return "Invalid arguments number.";
            }

            // Get parameters
            $planetName = $args['planetName'];
            $newOrbit = (int) $args['newOrbit'];

            $configHelper = new \Gnemes\MercadoLibre\SolarSystem\Helper\Config();

            $dbInfo = $configHelper->get("database");
            $config = new \Doctrine\DBAL\Configuration();

            $connectionParams = array(
                'dbname' => $dbInfo['database'],
                'user' => $dbInfo['username'],
                'password' => $dbInfo['password'],
                'host' => $dbInfo['hostname'],
                'port' => 3306,
                'charset' => 'utf8',
                'driver' => 'pdo_mysql',
            );
            $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

            $sql = <<<EOQ
SELECT * 
  FROM planeta WHERE name = :name
EOQ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('name', $planetName);
            $stmt->execute();
            $planet = $stmt->fetch();

            if ($planet !== false) {
                $sql = <<<EOQ
UPDATE planeta SET sun_distance = :newOrbit WHERE name = :name
EOQ;

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(":name", $planetName);
                $stmt->bindValue(":newOrbit", $newOrbit);
                $stmt->execute();

                // Delete records from clima table
                for ($i = 0; $i <= 3600; $i++) {
                    $conn->delete('clima', array('dia' => $i));
                }

                return "Orbit for planet ".$planetName." changed to: ".$newOrbit.". Please re-run job to update clima table.";
            } else {
                return "Invalid planet name: ".$planetName;
            }
        } else {
            return "Only accepts POST requests";
        }
    }
}