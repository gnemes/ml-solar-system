<?php

namespace Gnemes\MercadoLibre\SolarSystem\Helper;


class Geometry
{
    // Error acceptance
    const EPSILON_ERROR=0.00001;

    /**
     * Converts polar coordinates into rectangular ones
     * @param Array $polar
     * @return $this
     */
    public function polar2rectangularCoordinates($polar)
    {
        /**
         * Reference: http://repositorio.pucp.edu.pe/index/bitstream/handle/123456789/28688/introduccion_al_analisis_cap04.pdf?sequence=10
         *
         * x = r . cos(ang)
         * y = r . sen(ang)
         */

        $x = round($polar[0] * cos($polar[1]),4);
        $y = round($polar[0] * sin($polar[1]),4);

        $rectangularCoordinates = [$x, $y];

        return $rectangularCoordinates;
    }

    /**
     * Generate polar coordinates array from radius and angle in degrees
     *
     * @param int $radius
     * @param int $degrees
     * @return array
     */
    public function createPolarCoordinates($radius, $degrees)
    {
        // Convert angle from degrees to radians
        $radians = deg2rad($degrees);

        // Generate polar coordinates array
        $polarCoordinates = [$radius, $radians];

        return $polarCoordinates;
    }

    /**
     * Check if dots are aligned
     * @param array $dots
     * @param float $x1
     * @param float $y1
     * @param float $x2
     * @param float $y2
     * @return bool
     */
    public function areDotsAligned($dots, $x1 = null, $y1 = null, $x2 = null, $y2 = null)
    {
        if (empty($dots)) {
            // Cut condition. Aligned.
            return true;
        } else {
            // Get next point
            list($x, $y) = array_pop($dots);
            if (is_null($x1)) {
                return $this->areDotsAligned($dots, $x, $y);
            } else if (is_null($x2)) {
                return $this->areDotsAligned($dots, $x1, $y1, $x, $y);
            } else {
                $part1 = (($x2 - $x1) * ($y - $y1));
                $part2 = (($y2 - $y1) * ($x - $x1));

                //if ((($x2 - $x1) * ($y - $y1)) != (($y2 - $y1) * ($x - $x1))) {
                if ( abs($part1 - $part2) > self::EPSILON_ERROR) {
                    return false;
                }
                return $this->areDotsAligned($dots, $x1, $y1, $x2, $y2);
            }
        }
    }

    /**
     * Is dot inside a triangle
     *
     * @param float $x1
     * @param float $y1
     * @param float $x2
     * @param float $y2
     * @param float $x3
     * @param float $y3
     * @param float $x
     * @param float $y
     *
     * @return bool
     */
    public function dotInsideTriangle($x1, $y1, $x2, $y2, $x3, $y3, $x, $y)
    {
        $epsilon = 0.00001;

        /**
         * Given ABC triangle, a P dot is inside of it, if:
         * area(ABC) = area(ABP) + area(APC) + area(PBC)
         */

        // Calculating ABC area
        $areaTriangle = $this->calculateTriangleArea($x1, $y1, $x2, $y2, $x3, $y3);

        // Calculating ABP area
        $areaOne = $this->calculateTriangleArea($x1, $y1, $x2, $y2, $x, $y);

        // Calculating APC area
        $areaTwo = $this->calculateTriangleArea($x1, $y1, $x, $y, $x3, $y3);

        // Calculating PBC area
        $areaThree = $this->calculateTriangleArea($x, $y, $x2, $y2, $x3, $y3);

        // area(ABP) + area(APC) + area(PBC)
        $sum = $areaOne + $areaTwo + $areaThree;

        // Checking if areas are the same, with a given deviation
        $inside = (abs ($areaTriangle - $sum) < self::EPSILON_ERROR);

        return $inside;
    }

    /**
     * Area of a triangle
     *
     * @param float $x1
     * @param float $y1
     * @param float $x2
     * @param float $y2
     * @param float $x3
     * @param float $y3
     *
     * @return number
     */
    public function calculateTriangleArea($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $area = abs( (( $x1 * ($y2 - $y3) ) + ( $x2 * ($y3 - $y1) ) + ( $x3 * ($y1 - $y2) )) / 2);
        //$area = round($area, 2);
        return $area;
    }

    /**
     * Perimeter of a triangle
     *
     * @param $x1
     * @param $y1
     * @param $x2
     * @param $y2
     * @param $x3
     * @param $y3
     * @return float
     */
    public function calculateTrianglePerimeter($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $part1 = sqrt ( ($x1 - $x2) ** 2 + ($y1 - $y2) ** 2);
        $part2 = sqrt ( ($x2 - $x3) ** 2 + ($y2 - $y3) ** 2 );
        $part3 = sqrt ( ($x3 - $x1) ** 2 + ($y3 - $y1) ** 2 );
        $perimeter = $part1 + $part2 + $part3;
        return $perimeter;
    }
}
