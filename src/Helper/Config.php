<?php

namespace Gnemes\MercadoLibre\SolarSystem\Helper;

use Symfony\Component\Yaml\Yaml;

class Config
{
    private $config;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $configFile = __DIR__.'/../../config.yaml';
        $this->config = Yaml::parseFile($configFile);
    }

    /**
     * Get config by key
     * @param $key
     * @return array
     */
    public function get($key)
    {
        return isset($this->config[$key]) ? $this->config[$key] : [];
    }
}