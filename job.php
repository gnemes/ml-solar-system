<?php

require __DIR__ . '/vendor/autoload.php';

$solarSystem = new \Gnemes\MercadoLibre\SolarSystem\SolarSystem();
$configHelper = new \Gnemes\MercadoLibre\SolarSystem\Helper\Config();

$dbInfo = $configHelper->get("database");
$config = new \Doctrine\DBAL\Configuration();

$connectionParams = array(
    'dbname' => $dbInfo['database'],
    'user' => $dbInfo['username'],
    'password' => $dbInfo['password'],
    'host' => $dbInfo['hostname'],
    'port' => 3306,
    'charset' => 'utf8',
    'driver' => 'pdo_mysql',
);
$conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

$conn->delete('precipitaciones', array('1' => 1));
$conn->delete('clima', array('1' => 1));

for ($i = 0; $i <= 3600; $i++) {
    $solarSystem->moveToDay($i);

    $weather = $solarSystem->getCurrentWeatherCondition();

    if ($weather != 'Normal') {
        $conn->insert('clima', array('dia' => $i, 'clima' => $weather));

        if ($weather == 'Lluvia') {
            $conn->insert(
                'precipitaciones',
                array(
                    'dia' => $i,
                    'perimetro' => $solarSystem->getPlanetsTrianglePerimeter()
                )
            );
        }
    }
}

echo "Calculo de las predicciones climaticas del sistema solar por los proximos 10 años finalizado.";