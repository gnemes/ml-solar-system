<?php
require __DIR__ . '/vendor/autoload.php';

$solarSystem = new \Gnemes\MercadoLibre\SolarSystem\SolarSystem();

$weatherQty = $solarSystem->getWeatherConditionQtyByWeather();
$precipitationsMax = $solarSystem->getPrecipitationMaxDays();
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<html lang="en">
    <head>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Condiciones climaticas (expresadas en dias)</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <table class="table">
                        <tr>
                            <th>Lluvia</th>
                            <th>Sequia</th>
                            <th>Normal</th>
                            <th>Opt. Presion y Temp.</th>
                        </tr>
                        <tr>
                            <td><?php echo $weatherQty['Lluvia'];?></td>
                            <td><?php echo $weatherQty['Sequia'];?></td>
                            <td><?php echo $weatherQty['Normal'];?></td>
                            <td><?php echo $weatherQty['Condiciones optimas de presion y temperatura'];?></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Dias de pico maximo de precipitaciones</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="list-unstyled">
                        <?php
                        foreach ($precipitationsMax as $p) {
                            ?>
                            <li><?php echo $p['dia'];?></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>

        </div>
    </body>
</html>