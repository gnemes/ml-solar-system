<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


<script src="./assets/two.js"></script>
<script src="./assets/jquery-3.3.1.min.js"></script>

<html>
    <head>

    </head>
    <body>
        <div class="row">
            <div class="col-md-6 text-center">
                <div id="draw-shapes">
                </div>
            </div>

            <div class="col-md-6 text-center">
                <div id="draw-data">
                    <h1>Day</h1>
                    <p id="day-number"></p>

                    <h1>Go to day</h1>
                    <p>
                        <select id="go-to-date">
                            <?php
                            for ($i = 0; $i <= 3600; $i++) {
                                ?>
                                <option value="<?php echo $i; ?>">
                                    <?php echo $i; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                        <button id="go-to-date-button" class="btn btn-info">Go!</button>
                        <button id="play-from-day-button" class="btn btn-primary">Play</button>
                    </p>

                    <h1>Weather Condition</h1>
                    <p id="weather-condition"></p>

                    <h1>Parameters</h1>
                    <p class="text-info">
                    <ul class="list-unstyled">
                        <li>
                            <label for="draw-orbits">Draw Orbits</label>
                            <input type="checkbox" name="draw-orbits" id="draw-orbits" checked="checked"/>
                        </li>
                        <li>
                            <label for="draw-orbits">Draw Planet Names</label>
                            <input type="checkbox" name="draw-planet-names" id="draw-planet-names" checked="checked"/>
                        </li>
                        <li>
                            <label for="draw-axis">Draw Axis</label>
                            <input type="checkbox" name="draw-axis" id="draw-axis" checked="checked"/>
                        </li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>

<script type="application/javascript">
    function getData(day)
    {
        $.get( "api/v1/state/"+day, function( data ) {
            drawSolarSystem(data.planets);
            updateInformation(data);
        });
    }

    function updateInformation(data)
    {
        $('#day-number').html(data.day);
        $('#weather-condition').html(data.weather);
        $("#go-to-date").val(data.day);

        if (data.weather != 'Normal') {
            timeInterval = 1000;
        } else {
            timeInterval = 1000;
        }
    }

    function drawSolarSystem(planets) {
        //console.log(planets);

        $('#draw-shapes').html("");

        // Make an instance of two and place it on the page.
        var elem = document.getElementById('draw-shapes');
        var params = {width: 1000, height: 1000};
        var two = new Two(params).appendTo(elem);

        // Center coordinates
        var centerX = 250;
        var centerY = 250;

        // Lines vars
        var lineStrokeWidth = 1;

        // Planets vars
        var planetRadius = 1;
        var planetNameDraw = document.getElementById('draw-planet-names').checked;
        var orbitDraw = document.getElementById('draw-orbits').checked;

        // Sun vars
        var sunRadius = 1;

        // Axis vars
        var maxAxisX = 500;
        var maxAxisY = 500;
        var axisDraw = document.getElementById('draw-axis').checked;

        var firstDotX = false;
        var firstDotY = false;
        var previousDotX = false;
        var previousDotY = false;
        $.each(planets, function( index, p ) {
            //console.log(p);
            var radius = p.sunDistance / 10;
            var x = p.posX / 10 + centerX;
            var y = -1 * p.posY / 10 + centerY;

            if (orbitDraw) {
                var orbit = two.makeCircle(centerX, centerY, radius);
                orbit.stroke = 'black';
                orbit.linewidth = 1;
                orbit.noFill();
            }

            var planet = two.makeCircle(x , y, planetRadius);
            planet.fill = 'red';
            planet.stroke = 'red';
            planet.linewidth = 1;

            if (planetNameDraw) {
                var planetName = two.makeText(p.name, x,  y - 20);
                planetName.stroke = 'red';
            }

            if (previousDotX !== false) {
                var segm = two.makeLine(previousDotX, previousDotY, x, y);
                segm.stroke = 'blue';
                segm.linewidth = 1;
            } else {
                firstDotX = x;
                firstDotY = y;
            }

            previousDotX = x;
            previousDotY = y;
        });

        var segm = two.makeLine(previousDotX, previousDotY, firstDotX, firstDotY);
        segm.stroke = 'blue';
        segm.linewidth = lineStrokeWidth;


        if (axisDraw) {
            // Axis X
            var axisX = two.makeLine(0, centerY, maxAxisX, centerY);
            axisX.stroke = 'red';
            axisX.linewidth = 1;

            // Axis Y
            var axisY = two.makeLine(centerX, 0, centerX, maxAxisY);
            axisY.stroke = 'red';
            axisY.linewidth = 1;
        }

        // Sun
        var sunName = two.makeText("Sun", 0,  -20);
        sunName.stroke = '#FF8000';

        var sun = two.makeCircle(centerX, centerY, sunRadius);
        sun.fill = '#FFF000';
        /*
        sun.stroke = '#FF8000';
        sun.linewidth = 2;
        */

        two.update();
    }

    $("#go-to-date-button").click(function() {
        loopDays = false;
        var selectedDay = $("#go-to-date").val();
        getData(selectedDay);
    });

    $('#play-from-day-button').click(function() {
        if (loopDays) {
            $('#play-from-day-button').html('Play');
            $("#go-to-date-button").removeAttr("disabled");
        } else {
            $('#play-from-day-button').html('Stop')
            $("#go-to-date-button").attr("disabled", "");
        }
        loopDays = !loopDays;

        if (loopDays) {
            day = parseInt($("#go-to-date").val());
            getDataInterval();
        }
    });

    var loopDays = false;
    var day = 0;
    var timeInterval = 1000;

    function getDataInterval() {
        getData(day);
        day = day + 1;

        if ((day < 3600) && (loopDays))  {
            setTimeout(getDataInterval, timeInterval);
        }
    }

</script>